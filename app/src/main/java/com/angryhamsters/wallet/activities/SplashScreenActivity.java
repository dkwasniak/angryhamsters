package com.angryhamsters.wallet.activities;

import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.login.LoginActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashScreenActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);

		new android.os.Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(SplashScreenActivity.this,
						LoginActivity.class);
				startActivity(intent);
			}
		}, 0);

	}
}
