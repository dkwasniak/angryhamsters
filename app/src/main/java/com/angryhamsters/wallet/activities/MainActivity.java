package com.angryhamsters.wallet.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.widget.Toast;

import com.angryhamsters.wallet.AppPreferences;
import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.api.ApiConstants;
import com.angryhamsters.wallet.api.ApiData;
import com.angryhamsters.wallet.api.ApiError;
import com.angryhamsters.wallet.api.ApiParser;
import com.angryhamsters.wallet.api.ApiRequest;
import com.angryhamsters.wallet.api.ApiResponse;
import com.angryhamsters.wallet.api.enumeration.EHttp;
import com.angryhamsters.wallet.api.enumeration.ERequest;
import com.angryhamsters.wallet.api.enumeration.ResponseType;
import com.angryhamsters.wallet.api.models.Wallet;
import com.angryhamsters.wallet.base.BaseActivity;
import com.angryhamsters.wallet.base.BaseFragment;
import com.angryhamsters.wallet.base.FragmentType;
import com.angryhamsters.wallet.base.FragmentsFactory;
import com.angryhamsters.wallet.dialogs.WalletDetailsFragment;
import com.angryhamsters.wallet.fragments.PayoutDialog;
import com.angryhamsters.wallet.fragments.AddWalletDialog;
import com.angryhamsters.wallet.fragments.MyWalletsFragment;
import com.angryhamsters.wallet.fragments.PaymentDialog;
import com.angryhamsters.wallet.fragments.TransferDialog;
import com.angryhamsters.wallet.login.LoginActivity;
import com.angryhamsters.wallet.utils.GlobalUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.angryhamsters.wallet.api.enumeration.ResponseType.*;
import static com.angryhamsters.wallet.base.FragmentType.MY_WALLETS_FRAGMENT;
import static com.angryhamsters.wallet.base.FragmentType.WALLET_DETAILS_FRAGMENT;
import static java.util.Arrays.asList;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, ApiRequest.OnNetworkListener,
        PaymentDialog.PaymentDialogCallback, AddWalletDialog.AddWalletDialogCallback,
        PayoutDialog.PayoutDialogCallback, WalletDetailsFragment.WalletDetailFragmentCallback,
        TransferDialog.TransferDialogCallback{


    private static final String BASE_FRAGMENT_TAG = "base_fragment_tag";

    private FragmentType currentFragmentType = MY_WALLETS_FRAGMENT;
    private BaseFragment currentFragment;
    private Wallet wallet;

    public Wallet getWallet() {
        return wallet;
    }

    private ArrayList<String> myWallets;
    private float amountToReload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        setMainFragment();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(!currentFragment.getFragmentType().equals(MY_WALLETS_FRAGMENT)) {
                super.onBackPressed();
            }
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.my_wallets) {
            showFragment(MY_WALLETS_FRAGMENT);
        } else if (id == R.id.nav_gallery) {
            // Tutaj mozna zaladowac inny fragment
        } else if (id == R.id.logout) {
            showLogoutDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void setMainFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        BaseFragment fragment = FragmentsFactory.getFragmentByType(MY_WALLETS_FRAGMENT);
        ft.replace(R.id.main_container, fragment, BASE_FRAGMENT_TAG).addToBackStack(BASE_FRAGMENT_TAG)
                .commit();
        currentFragmentType = fragment.getFragmentType();
        currentFragment = fragment;
    }


    private void showLogoutDialog() {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.Base_Theme_AppCompat_Light_Dialog_Alert);
        builder.setTitle("Wyloguj");
        builder.setMessage("Jesteś pewny ?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
            }
        });
        builder.setNegativeButton("Anuluj", null);
        builder.show();
    }

    private void logout() {
        new ApiRequest(this,
                new ApiData(ApiConstants.getUserLogout(), null, EHttp
                        .GET, ERequest.USER_LOGOUT_REQUEST), OBJECT, this).execute();
        AppPreferences.getInstance().setUserCookie("");
    }

    @Override
    public void onRequestSuccess(ApiResponse response) {
        switch (response.getRequestType()) {
            case USER_LOGOUT_REQUEST:
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                AppPreferences.getInstance().setUserCookie("");
                break;
            case PAYOUT_REQUEST:
                reloadWallet();
                break;
            case ADD_WALLET_REQUEST:
                MyWalletsFragment fragment = (MyWalletsFragment) currentFragment;
                if (fragment != null) {
                    fragment.getAdapter().add(wallet);
                }
                break;
            case PAYMENT_REQUEST:
                reloadWallet();
                break;
            case GET_WALLETS_REQUEST:
                List<Wallet> values = ApiParser.getWalletsFromResponse(response.getResponse());
                setMyWallets(values);
                if(currentFragment.getFragmentType().equals(WALLET_DETAILS_FRAGMENT)) {
                    WalletDetailsFragment frag = (WalletDetailsFragment) currentFragment;
                    if(removeCurrentWallet(frag.getCurrentWallet())){
                        frag.showTransferDialog(myWallets);
                    }
                }
                break;
            case TRANSFER_REQUEST:
                reloadWallet();
                break;
        }
    }

    private boolean removeCurrentWallet(Wallet wallet) {
        for(int i=0;i<myWallets.size();i++){
            if(myWallets.get(i).equals(wallet.getName())) {
                myWallets.remove(i);
            }
        }
        if(myWallets.isEmpty()) {
            Toast.makeText(this, "Masz tylko jeden portfel!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void reloadWallet() {
        if(currentFragment.getFragmentType().equals(WALLET_DETAILS_FRAGMENT)) {
            WalletDetailsFragment detailsFragment = (WalletDetailsFragment) currentFragment;
            if (detailsFragment != null) {
                detailsFragment.reloadData();
            }
        }
    }

    @Override
    public void onRequestFailed(ApiError error) {
        Toast.makeText(this, error.getErrorMessage(), Toast.LENGTH_LONG).show();
    }

    private void setMyWallets(List<Wallet> values) {
        if(values != null) {
            myWallets = new ArrayList<>();
            for (int i = 0; i < values.size(); i++) {
                myWallets.add(values.get(i).getName());
            }
        }
    }

    @Override
    public void onSendPayoutRequest(Wallet wallet, String amount, String account) {
        payout(wallet, amount);
    }



    private void payout(Wallet wallet, String amount) {
        Map<String, String> params = GlobalUtils.getRequestParams(
                asList("walletName", "amount"),
                asList(wallet.getName(), amount));
        new ApiRequest(this,
                new ApiData(ApiConstants.getPayout(), params, EHttp
                        .POST, ERequest.PAYOUT_REQUEST), OBJECT, this).execute();
    }

    private void showFragment(FragmentType fragmentType) {
        if (!currentFragmentType.equals(fragmentType)) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.main_container, FragmentsFactory.getFragmentByType(fragmentType),
                    BASE_FRAGMENT_TAG).addToBackStack(BASE_FRAGMENT_TAG).commit();
        }
    }

    private void showFragment(BaseFragment fragment) {
        if (!currentFragmentType.equals(fragment.getFragmentType())) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.main_container, fragment,
                    BASE_FRAGMENT_TAG).addToBackStack(BASE_FRAGMENT_TAG).commit();
            currentFragment = fragment;
        }

        if(currentFragment.equals(MY_WALLETS_FRAGMENT)) {

        } else {

        }
    }

    @Override
    public void onChangeFragment(FragmentType type) {
        showFragment(type);
    }

    @Override
    public void onChangeFragment(BaseFragment fragment) {
        currentFragment = fragment;
        showFragment(fragment);
    }

    @Override
    public void onSendPaymentRequest(Double amount, String walletName) {
        Map<String, String> params = GlobalUtils.getRequestParams(
                asList("walletName", "amount"),
                asList(walletName, amount.toString()));
        new ApiRequest(this,
                new ApiData(ApiConstants.getPayment(), params, EHttp
                        .POST, ERequest.PAYMENT_REQUEST), ResponseType.OBJECT, this).execute();
    }

    @Override
    public void onSendAddWalletRequest(Wallet wallet) {
        this.wallet = wallet;
        Map<String, String> params = GlobalUtils.getRequestParams(
                asList("name", "currency", "amount"),
                asList(wallet.getName(), wallet.getCurrency(), wallet.getAmount().toString()));
        new ApiRequest(this,
                new ApiData(ApiConstants.getAddWallet(), params, EHttp
                        .POST, ERequest.ADD_WALLET_REQUEST), ResponseType.OBJECT, this).execute();

    }

    @Override
    public void onGetWallets() {
        new ApiRequest(this,
                new ApiData(ApiConstants.getGetWallets(), new HashMap<String, String>(), EHttp
                        .GET, ERequest.GET_WALLETS_REQUEST), ResponseType.ARRAY, this).execute();
    }

    @Override
    public void onTransferRequest(String walletFrom, String walletTo, String amount) {
        Map<String, String> params = GlobalUtils.getRequestParams(
                asList("walletFromName", "walletToName", "amount"),
                asList(walletFrom, walletTo, amount.toString()));
        new ApiRequest(this,
                new ApiData(ApiConstants.getTransfer(), params, EHttp
                        .POST, ERequest.TRANSFER_REQUEST), ResponseType.OBJECT, this).execute();
    }


    private void reloadDetailsFragment(float amount) {
        if(currentFragment.getFragmentType().equals(WALLET_DETAILS_FRAGMENT)) {
            ((WalletDetailsFragment) currentFragment).reload(amount);
        }
    }
}
