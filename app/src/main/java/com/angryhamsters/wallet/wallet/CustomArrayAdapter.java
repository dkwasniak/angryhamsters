package com.angryhamsters.wallet.wallet;

import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.api.models.Wallet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomArrayAdapter extends ArrayAdapter<Wallet> {
    private final Context context;
    private final List<Wallet> values;

    public CustomArrayAdapter(Context context, List<Wallet> values) {
        super(context, R.layout.wallet_list, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.wallet_list, parent, false);
        TextView name = (TextView) rowView.findViewById(R.id.wallet_name_line);
        name.setText(values.get(position).getName());
        TextView amount = (TextView) rowView.findViewById(R.id.wallet_amount_line);
        amount.setText(values.get(position).getAmount().toString());
        TextView currency = (TextView) rowView.findViewById(R.id.wallet_currency_line);
        currency.setText(values.get(position).getCurrency());

        return rowView;
    }
}
