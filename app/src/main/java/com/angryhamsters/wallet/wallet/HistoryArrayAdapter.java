package com.angryhamsters.wallet.wallet;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.api.models.PayTransaction;
import com.angryhamsters.wallet.api.models.Transaction;
import com.angryhamsters.wallet.api.models.TransferTransaction;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class HistoryArrayAdapter extends ArrayAdapter<Transaction> {

    private final Context context;
    private final List<Transaction> transactions;
    private String walletName;

    TextView date;
    TextView amount;
    TextView balance;
    TextView description;

    public HistoryArrayAdapter(Context context, List<Transaction> transactions, String walletName) {
        super(context, R.layout.wallet_list, transactions);
        this.context = context;
        this.transactions = transactions;
        this.walletName = walletName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.transaction_row, parent, false);
        date = (TextView) rowView.findViewById(R.id.transaction_date);
        amount = (TextView) rowView.findViewById(R.id.transaction_amount);
        balance = (TextView) rowView.findViewById(R.id.transaction_balance);
        description = (TextView) rowView.findViewById(R.id.transaction_description);

        switch (transactions.get(position).getType()) {
            case 0:
                PayTransaction pt0 = (PayTransaction) transactions.get(position);
                setFields(pt0.getDate(), pt0.getAmount(), pt0.getAmountAfterOperation(), "Wpłata", true);
                break;
            case 1:
                PayTransaction pt = (PayTransaction) transactions.get(position);
                setFields(pt.getDate(), pt.getAmount(), pt.getAmountAfterOperation(), "Wypłata", false);
                break;
            case 2:
                TransferTransaction t = (TransferTransaction) transactions.get(position);
                if (t.getFromWallet().equals(walletName)) {
                    setFields(t.getDate(), t.getAmount(), t.getAmountAfterOperationOnFirstWallet(),
                            "Przelew do portfela " + t.getToWallet(), false);
                } else {
                    setFields(t.getDate(), t.getAmount(), t.getAmountAfterOperationOnFirstWallet(),
                            "Przelew z portfela" + t.getFromWallet(), true);
                }
                break;
        }

        return rowView;
    }

    private void setFields(Date dateValue, Double amountValue, Double balanceValue, String descriptionText, boolean payment) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        date.setText(dateFormat.format(dateValue));
        balance.setText((balanceValue.toString()));
        description.setText(descriptionText);
        if (payment) {
            amount.setText("+" + amountValue.toString());
            amount.setTextColor(Color.GREEN);
        } else {
            amount.setText("-" + amountValue.toString());
            amount.setTextColor(Color.RED);
        }
    }
}
