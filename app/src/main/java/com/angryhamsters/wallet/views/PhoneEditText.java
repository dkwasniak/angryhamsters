package com.angryhamsters.wallet.views;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Damian Kwasniak on 31.05.15.
 */
public class PhoneEditText extends EditText {

	public PhoneEditText(final Context context) {
		super(context);
		init();
	}

	public PhoneEditText(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public PhoneEditText(final Context context, final AttributeSet attrs, final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	private void init() {
		addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(final CharSequence s, final int start, final int count,
					final int after) {

			}

			@Override
			public void onTextChanged(final CharSequence s, final int start, final int before,
					final int count) {
				if(s.length()< 9) {
					setTextColor(Color.RED);
				} else {
					setTextColor(Color.BLACK);
				}

			}

			@Override
			public void afterTextChanged(final Editable s) {

			}
		});
	}





}
