package com.angryhamsters.wallet.base;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Damian Kwasniak on 08.06.15.
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseFragment.BaseFragmentListener {

	public abstract void setMainFragment();

}
