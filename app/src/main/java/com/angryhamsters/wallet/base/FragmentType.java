package com.angryhamsters.wallet.base;

/**
 * Created by Damian Kwasniak on 08.06.15.
 */
public enum FragmentType {
	// login
	USER_LOGIN_FRAGMENT,
	USER_REGISTER_FRAGMENT,
	MY_WALLETS_FRAGMENT,
	WALLET_DETAILS_FRAGMENT,
	PAYMENT_FRAGMENT
}
