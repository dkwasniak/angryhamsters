package com.angryhamsters.wallet.base;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;


public abstract class BaseFragment extends Fragment {

	public interface BaseFragmentListener {
		void onChangeFragment(FragmentType type);
		void onChangeFragment(BaseFragment fragment);
	}

	public BaseFragmentListener baseFragmentListener;


	@Override
	public void onAttach(final Context context) {
		super.onAttach(context);
		baseFragmentListener = (BaseActivity) context;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		baseFragmentListener = null;
	}

	public abstract FragmentType getFragmentType();


	protected BaseFragmentListener getBaseFragmentListener() {
		return baseFragmentListener;
	}

	protected void setBaseFragmentListener(
			final BaseFragmentListener baseFragmentListener) {
		this.baseFragmentListener = baseFragmentListener;
	}

}

