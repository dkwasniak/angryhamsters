package com.angryhamsters.wallet.base;

import com.angryhamsters.wallet.fragments.MyWalletsFragment;
import com.angryhamsters.wallet.login.fragments.UserLoginFragment;
import com.angryhamsters.wallet.login.fragments.UserRegisterFragment;

import android.content.res.Resources;

/**
 * Created by Damian Kwasniak on 09.06.15.
 */
public class FragmentsFactory {

	public static BaseFragment getFragmentByType(FragmentType type) {
		switch(type) {
			case USER_LOGIN_FRAGMENT:
				return UserLoginFragment.newInstance();

			case USER_REGISTER_FRAGMENT:
				return UserRegisterFragment.newInstance();
			case MY_WALLETS_FRAGMENT:
				return MyWalletsFragment.getInstance();
			default:
				throw new Resources.NotFoundException("Unable to find fragment with "
						+ "delivered fragment type:" + type.toString());
		}
	}

}
