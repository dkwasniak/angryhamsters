package com.angryhamsters.wallet;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import android.app.Application;
import android.text.TextUtils;

/**
 * Created by Damian Kwasniak on 30.05.15.
 */
public class WalletApplication extends Application {

	public static final String TAG = WalletApplication.class.getSimpleName();

	private static WalletApplication instance;

	private RequestQueue requestQueue;

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		AppPreferences.initializeInstance(this);
	}

	public static synchronized WalletApplication getInstance() {
		return instance;
	}

	public RequestQueue getRequestQueue() {
		if (requestQueue == null) {
			requestQueue = Volley.newRequestQueue(getApplicationContext());
		}
		return requestQueue;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (requestQueue != null) {
			requestQueue.cancelAll(tag);
		}
	}
}
