package com.angryhamsters.wallet.api;

import com.angryhamsters.wallet.api.enumeration.ERequest;

/**
 * Created by Damian Kwasniak on 31.05.15.
 */
public class ApiResponse {

	private String response;

	private ERequest request;

	public ApiResponse(String response, ERequest request) {
		this.response = response;
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(final String response) {
		this.response = response;
	}

	public ERequest getRequestType() {
		return request;
	}

	public void setRequestType(final ERequest request) {
		this.request = request;
	}
}
