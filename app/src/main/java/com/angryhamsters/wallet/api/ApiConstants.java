package com.angryhamsters.wallet.api;

/**
 * Created by Damian Kwasniak on 08.06.15.
 */
public class ApiConstants {

	private static final String SERVER_ADDRESS = "http://46.101.136.26/";

	private static final String USER_REGISTER = "user/register";

	private static final String USER_LOGIN = "user/login";

	private static final String USER_LOGOUT = "user/logout";

	private static final String GET_WALLETS = "wallet/all";

	private static final String GET_WALLET = "wallet/";

	private static final String GET_CURRENCY = "wallet/currency";

	private static final String ADD_WALLET = "wallet";

	private static final String PAYMENT = "transactions/payment";

	private static final String PAYOUT = "transactions/withdraw";

    private static final String TRANSFER = "transactions/transfer";

	private static final String TRANSACTIONS = "transactions/";


	public static String getServerAddress() { return SERVER_ADDRESS; }

	public static String getUserRegister() {
		return USER_REGISTER;
	}

	public static String getUserLogin() {
		return USER_LOGIN;
	}

	public static String getUserLogout() {
		return USER_LOGOUT;
	}

	public static String getGetWallets() {
		return GET_WALLETS;
	}

	public static String getGetWallet() { return  GET_WALLET; }

	public static String getGetCurrency() { return GET_CURRENCY; }

	public static String getAddWallet() { return ADD_WALLET; }

	public static String getPayment() {return  PAYMENT; }

	public static String getPayout() { return PAYOUT; }

    public static String getTransfer() { return TRANSFER; }

	public static String getTransactions() {return TRANSACTIONS; }
}
