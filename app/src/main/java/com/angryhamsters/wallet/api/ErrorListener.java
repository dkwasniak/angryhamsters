package com.angryhamsters.wallet.api;

import com.android.volley.Response;
import com.android.volley.VolleyError;

/**
 * Copyright (c) Redwood
 * <p/>
 * Created by:
 * Damian
 * d.kwasniak@redwood.io
 * <p/>
 * 03.11.2015
 */
public class ErrorListener implements Response.ErrorListener {
    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
