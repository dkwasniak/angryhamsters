package com.angryhamsters.wallet.api;

import com.angryhamsters.wallet.api.models.PayTransaction;
import com.angryhamsters.wallet.api.models.Transaction;
import com.angryhamsters.wallet.api.models.TransferTransaction;
import com.angryhamsters.wallet.api.models.Wallet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import com.angryhamsters.wallet.api.models.User;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import java.lang.reflect.Type;

/**
 * Created by Damian Kwasniak on 09.06.15.
 */
public class ApiParser {

	private static Gson gson = new Gson();

	public static User getUserFromResponse(String response) {
		gson = new Gson();
		User user = gson.fromJson(getJsonElementByName(response, "user"),User.class);

		return user;
	}

	public static List<Wallet> getWalletsFromResponse(String response){
		Type listType = new TypeToken<ArrayList<Wallet>>(){}.getType();
		List<Wallet> wallets = gson.fromJson(response.trim(), listType);;
		return wallets;
	}

	public static Wallet getWalletFromResponse(String response){
		Wallet wallet = gson.fromJson(response.trim(), Wallet.class);;
		return wallet;
	}

	public static List<Transaction> getTransactionFromResponse(String response) throws JSONException{
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("hh:mm dd.MM.yyyy");

		List<Transaction> transactions = new ArrayList<>();
		JSONArray jsonTransactions = new JSONArray(response);
		for (int i = 0; i < jsonTransactions.length(); i++) {
			JSONObject object = jsonTransactions.getJSONObject(i);
			if (object.getInt("type") == 2) {
				TransferTransaction transferTransaction = gsonBuilder.create().fromJson(object.toString(), TransferTransaction.class);
				transactions.add(transferTransaction);
			} else {
				PayTransaction payTransaction = gsonBuilder.create().fromJson(object.toString(), PayTransaction.class);
				transactions.add(payTransaction);
			}
		}
		Collections.sort(transactions, new Comparator<Transaction>() {
			@Override
			public int compare(Transaction lhs, Transaction rhs) {
				if (lhs.getDate().before(rhs.getDate())) {
					return 1;
				} else if (lhs.getDate().after(rhs.getDate())) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		return transactions;
	}

	public static ErrorMessage getErrorMessageFromResponse(String responseMessage) {
		return gson.fromJson(responseMessage,ErrorMessage.class);
	}

	private static JsonElement getJsonElementByName(String response, String name) {
		JsonParser parser = new JsonParser();
		JsonObject element = (JsonObject)parser.parse(response);
		JsonElement responseWrapper = element.get(name);

		return responseWrapper;
	}
}
