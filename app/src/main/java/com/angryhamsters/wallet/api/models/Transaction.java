package com.angryhamsters.wallet.api.models;


import com.google.gson.annotations.Expose;

import java.util.Date;

public class Transaction {

    @Expose
    private Integer type;

    @Expose
    private Double amount;

    @Expose
    private Date date;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
