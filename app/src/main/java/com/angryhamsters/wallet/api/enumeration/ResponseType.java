package com.angryhamsters.wallet.api.enumeration;

public enum ResponseType {
    OBJECT,
    ARRAY
}
