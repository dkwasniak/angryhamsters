package com.angryhamsters.wallet.api;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.angryhamsters.wallet.AppPreferences;
import com.angryhamsters.wallet.api.enumeration.ERequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;

public class WalletJsonObjectRequest extends com.android.volley.toolbox.JsonObjectRequest {

    private ApiData apiData;

    public WalletJsonObjectRequest(int method, String url, JSONObject jsonRequest,
                                   Response.Listener<JSONObject> listener,
                                   Response.ErrorListener errorListener, ApiData apiData) {
        super(method, url, jsonRequest, listener, errorListener);
        this.apiData = apiData;
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            // here's the new code, if jsonString.length() == 0 don't parse
            if(apiData.getRequest().equals(ERequest.USER_LOGIN_REQUEST) && response.statusCode == 200) {
                Map<String, String > headers = response.headers;
                getCookieFromResponse(headers);
            }
            if (jsonString.length() == 0) {
                return Response.success(null, HttpHeaderParser.parseCacheHeaders(response));
            }
            // end of patch


            return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    private void getCookieFromResponse(Map<String, String > headers) {
        Iterator it = headers.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            if(pair.getKey().toString().equals("set-cookie")) {
                String cookie = pair.getValue().toString().split(";")[0];
                AppPreferences app = AppPreferences.getInstance();
                AppPreferences.getInstance().setUserCookie(cookie);
            }
            it.remove(); // avoids a ConcurrentModificationException
        }
    }
}
