package com.angryhamsters.wallet.api.models;

import com.google.gson.annotations.Expose;

public class PayTransaction extends Transaction {

    @Expose
    private String wallet;

    @Expose
    private Double amountAfterOperation;

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public Double getAmountAfterOperation() {
        return amountAfterOperation;
    }

    public void setAmountAfterOperation(Double amountAfterOperation) {
        this.amountAfterOperation = amountAfterOperation;
    }
}
