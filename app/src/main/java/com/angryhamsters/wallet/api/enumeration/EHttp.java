package com.angryhamsters.wallet.api.enumeration;

/**
 * Created by Damian Kwasniak on 30.05.15.
 */
public enum EHttp {
	GET,
	POST,
	PUT,
	DELETE,
	HEAD,
	OPTIONS,
	TRACE,
	PATCH
}
