package com.angryhamsters.wallet.api.models;


import com.google.gson.annotations.Expose;

public class TransferTransaction extends Transaction {

    @Expose
    private String fromWallet;

    @Expose
    private String toWallet;

    @Expose
    private Double amountAfterOperationOnFirstWallet;

    @Expose
    private Double amountAfterOperationOnSecondWallet;

    public Double getAmountAfterOperationOnFirstWallet() {
        return amountAfterOperationOnFirstWallet;
    }

    public void setAmountAfterOperationOnFirstWallet(Double amountAfterOperationOnFirstWallet) {
        this.amountAfterOperationOnFirstWallet = amountAfterOperationOnFirstWallet;
    }

    public String getFromWallet() {
        return fromWallet;
    }

    public void setFromWallet(String fromWallet) {
        this.fromWallet = fromWallet;
    }

    public String getToWallet() {
        return toWallet;
    }

    public void setToWallet(String toWallet) {
        this.toWallet = toWallet;
    }

    public Double getAmountAfterOperationOnSecondWallet() {
        return amountAfterOperationOnSecondWallet;
    }

    public void setAmountAfterOperationOnSecondWallet(Double amountAfterOperationOnSecondWallet) {
        this.amountAfterOperationOnSecondWallet = amountAfterOperationOnSecondWallet;
    }

}
