package com.angryhamsters.wallet.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.api.models.Wallet;

import java.util.ArrayList;

public class TransferDialog extends DialogFragment {




    public interface TransferDialogCallback {

        void onTransferRequest(String walletFrom, String walletTo ,String amount);

    }

    private static final String WALLET = "wallet";

    private static final String WALLETS = "wallets";

    private TransferDialogCallback callback;

    private Wallet wallet;

    private ArrayList<String> wallets;

    private EditText amountEditText;

    private Spinner currencySpinner;

    private ArrayAdapter<String> walletsAdapter;

    public static TransferDialog getInstance(Wallet wallet, ArrayList<String> wallets) {
        TransferDialog dialog = new TransferDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable(WALLET, wallet);
        bundle.putStringArrayList(WALLETS, wallets);
        dialog.setArguments(bundle);
        return dialog;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.wallet = getArguments().getParcelable(WALLET);
        this.wallets = getArguments().getStringArrayList(WALLETS);
        walletsAdapter = new ArrayAdapter<String>((Activity)callback,
                android.R.layout.simple_list_item_1,wallets);
    }



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.transfer_dialog, null, false);
        currencySpinner = (Spinner) rootView.findViewById(R.id.currencySpinner);
        amountEditText  = (EditText) rootView.findViewById(R.id.amountEditText);

        currencySpinner.setAdapter(walletsAdapter);

        return new AlertDialog.Builder((Activity) callback)
                .setTitle("Waluta")
                .setView(rootView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callback != null) {
                            if (validate()) {
                                callback.onTransferRequest(wallet.getName(),
                                        currencySpinner.getSelectedItem().toString(),
                                        amountEditText.getText().toString());
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.missing_data_string),
                                        Toast.LENGTH_LONG).show();
                            }

                        }

                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
    }

    private boolean validate() {
        return !amountEditText.getText().toString().equals("");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callback = (TransferDialogCallback) activity;
    }
}
