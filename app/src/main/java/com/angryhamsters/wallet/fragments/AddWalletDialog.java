package com.angryhamsters.wallet.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.api.models.Wallet;

public class AddWalletDialog extends DialogFragment {

    public interface AddWalletDialogCallback {

        void onSendAddWalletRequest(Wallet wallet);

    }

    private AddWalletDialogCallback callback;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Context ctx = getActivity();
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.add_wallet, null, false);
        final EditText newWalletName = (EditText) rootView.findViewById(R.id.add_wallet_name);
        final Spinner newWalletCurrency = (Spinner) rootView.findViewById(R.id.add_wallet_currency);

        return new AlertDialog.Builder(ctx)
                .setTitle("Dodaj portfel")
                .setView(rootView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String name = newWalletName.getText().toString();
                        String currency = String.valueOf(newWalletCurrency.getSelectedItem());
                        if (TextUtils.isEmpty(name)) {
                            newWalletName.setError(getString(R.string.add_wallet_name_error));
                            return;
                        } else {
                            if (callback != null) {
                                Wallet wallet = new Wallet(0, name, currency, 0.0);
                                callback.onSendAddWalletRequest(wallet);
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof AddWalletDialogCallback) {
            this.callback = (AddWalletDialogCallback) activity;
        }
    }

    @Override
    public void onDetach() {
        this.callback = null;
        super.onDetach();
    }
}

