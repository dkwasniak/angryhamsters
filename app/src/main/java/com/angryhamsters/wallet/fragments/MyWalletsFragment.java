package com.angryhamsters.wallet.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.api.ApiConstants;
import com.angryhamsters.wallet.api.ApiData;
import com.angryhamsters.wallet.api.ApiError;
import com.angryhamsters.wallet.api.ApiParser;
import com.angryhamsters.wallet.api.ApiRequest;
import com.angryhamsters.wallet.api.ApiResponse;
import com.angryhamsters.wallet.api.enumeration.EHttp;
import com.angryhamsters.wallet.api.enumeration.ERequest;
import com.angryhamsters.wallet.api.enumeration.ResponseType;
import com.angryhamsters.wallet.base.BaseFragment;
import com.angryhamsters.wallet.base.FragmentType;
import com.angryhamsters.wallet.dialogs.WalletDetailsFragment;
import com.angryhamsters.wallet.wallet.CustomArrayAdapter;
import com.angryhamsters.wallet.api.models.Wallet;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import static com.angryhamsters.wallet.base.FragmentType.MY_WALLETS_FRAGMENT;

public class MyWalletsFragment extends BaseFragment implements ApiRequest.OnNetworkListener {

    private static final String WALLET = "wallet";

    private ListView list;
    private CustomArrayAdapter adapter;
    private FloatingActionButton fab;


    public MyWalletsFragment() {
        // Required empty public constructor
    }

    public static BaseFragment getInstance() {
        return new MyWalletsFragment();
    }

    public CustomArrayAdapter getAdapter() {
        return adapter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_my_wallets, container, false);
        list=(ListView) view.findViewById(R.id.wallets_list);
        adapter = new CustomArrayAdapter(getActivity(), new ArrayList<Wallet>());
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Wallet wallet = (Wallet) list.getItemAtPosition(position);

                WalletDetailsFragment detailFragment = WalletDetailsFragment.getInstance(wallet);
                baseFragmentListener.onChangeFragment(detailFragment);
                fab.setVisibility(View.GONE);
            }

        });
        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AddWalletDialog dialog = new AddWalletDialog();
                    dialog.show(getActivity().getSupportFragmentManager(), "AddItemDialog");
                }
            });
        }
        fab.setVisibility(View.VISIBLE);
        new ApiRequest(getActivity(),
                new ApiData(ApiConstants.getGetWallets(), new HashMap<String, String>(), EHttp
                        .GET, ERequest.GET_WALLETS_REQUEST), ResponseType.ARRAY, this).execute();
        return view;
    }

    @Override
    public void onRequestSuccess(final ApiResponse response) {
        List<Wallet> values = ApiParser.getWalletsFromResponse(response.getResponse());
        adapter.addAll(values);
    }

    @Override
    public void onRequestFailed(final ApiError error) {
        Toast.makeText(getActivity(), error.getErrorMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public FragmentType getFragmentType() {
        return MY_WALLETS_FRAGMENT;
    }

}
