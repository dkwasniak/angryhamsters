package com.angryhamsters.wallet.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.api.models.Wallet;

public class PaymentDialog extends DialogFragment {

    public interface PaymentDialogCallback {

        void onSendPaymentRequest(Double amount, String walletName);

    }

    private PaymentDialogCallback callback;

    private static final String WALLET = "wallet";
    Wallet wallet;

    public static PaymentDialog getInstance(Wallet wallet) {
        PaymentDialog dialog = new PaymentDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable(WALLET, wallet);
        dialog.setArguments(bundle);
        return dialog;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.wallet = getArguments().getParcelable(WALLET);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Context ctx = getActivity();
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.payment, null, false);
        final EditText paymentAmount = (EditText) rootView.findViewById(R.id.payment_amount);

        return new AlertDialog.Builder(ctx)
                .setTitle("Wpłata")
                .setView(rootView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!TextUtils.isEmpty(paymentAmount.getText().toString())) {
                            Double amountToAdd = Double.parseDouble(paymentAmount.getText().toString());
                            if (callback != null) {
                                callback.onSendPaymentRequest(amountToAdd, wallet.getName());
                            }
                        } else {
                            paymentAmount.setError(getString(R.string.payment_amount_error));
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof PaymentDialogCallback) {
            this.callback = (PaymentDialogCallback) activity;
        }
    }

    @Override
    public void onDetach() {
        this.callback = null;
        super.onDetach();
    }

}

