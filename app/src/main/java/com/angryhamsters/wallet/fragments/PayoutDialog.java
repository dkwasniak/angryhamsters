package com.angryhamsters.wallet.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.api.models.Wallet;

public class PayoutDialog extends DialogFragment{

    private static final String WALLET = "wallet";

    public interface PayoutDialogCallback {

        void onSendPayoutRequest(Wallet walletName, String amount, String account);

    }

    private PayoutDialogCallback callback;

    private EditText amountEditText;
    private EditText accountEditText;

    private Wallet wallet;

    public static PayoutDialog getInstance(Wallet wallet) {
        PayoutDialog dialog = new PayoutDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable(WALLET, wallet);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.wallet = getArguments().getParcelable(WALLET);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Context ctx = getActivity();
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.payout_dialog, null, false);
        amountEditText = (EditText) rootView.findViewById(R.id.amountEditText);
        accountEditText = (EditText) rootView.findViewById(R.id.accountEditText);

        return new AlertDialog.Builder(ctx)
                .setTitle("Wypłata")
                .setView(rootView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callback != null) {
                            if (validate()) {
                                callback.onSendPayoutRequest(wallet,amountEditText.getText().toString(),
                                        accountEditText.getText().toString() );
                            } else {
                                Toast.makeText(getActivity(),getResources().getString(R.string.missing_data_string),
                                        Toast.LENGTH_LONG).show();
                            }

                        }

                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
    }

    private boolean validate() {
        return !amountEditText.getText().toString().equals("") &&
                !accountEditText.getText().toString().equals("");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.callback = (PayoutDialogCallback) activity;

    }

    @Override
    public void onDetach() {
        this.callback = null;
        super.onDetach();
    }
}
