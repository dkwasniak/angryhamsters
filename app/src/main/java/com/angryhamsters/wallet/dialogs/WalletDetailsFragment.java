package com.angryhamsters.wallet.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.angryhamsters.wallet.R;

import com.angryhamsters.wallet.api.ApiConstants;
import com.angryhamsters.wallet.api.ApiData;
import com.angryhamsters.wallet.api.ApiError;
import com.angryhamsters.wallet.api.ApiParser;
import com.angryhamsters.wallet.api.ApiRequest;
import com.angryhamsters.wallet.api.ApiResponse;

import com.angryhamsters.wallet.api.enumeration.EHttp;
import com.angryhamsters.wallet.api.enumeration.ERequest;
import com.angryhamsters.wallet.api.enumeration.ResponseType;
import com.angryhamsters.wallet.api.models.Transaction;
import com.angryhamsters.wallet.api.models.Wallet;
import com.angryhamsters.wallet.base.BaseFragment;
import com.angryhamsters.wallet.base.FragmentType;
import com.angryhamsters.wallet.fragments.PaymentDialog;
import com.angryhamsters.wallet.fragments.PayoutDialog;
import com.angryhamsters.wallet.fragments.TransferDialog;
import com.angryhamsters.wallet.wallet.HistoryArrayAdapter;


import org.json.JSONException;

import java.util.ArrayList;

import static com.angryhamsters.wallet.base.FragmentType.WALLET_DETAILS_FRAGMENT;


import java.util.HashMap;
import java.util.List;


public class WalletDetailsFragment extends BaseFragment implements View.OnClickListener,
        ApiRequest.OnNetworkListener {

    private static final String WALLET = "wallet";

    public interface WalletDetailFragmentCallback {
        void onGetWallets();


    }

    private Wallet wallet;

    private Button payoutButton;
    private Button paymentButton;

    private Button currencyButton;

    private TextView nameTextView;
    private TextView currencyTextView;
    private TextView amountTextView;

    private ListView historyList;
    private HistoryArrayAdapter adapter;

    private WalletDetailFragmentCallback callback;

    public static WalletDetailsFragment getInstance(Wallet wallet) {
        WalletDetailsFragment fragment = new WalletDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(WALLET,wallet);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wallet = getArguments().getParcelable(WALLET);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view= inflater.inflate(R.layout.wallet_details, container, false);

        nameTextView = (TextView) view.findViewById(R.id.wallet_name);
        currencyTextView = (TextView) view.findViewById(R.id.wallet_currency);
        amountTextView = (TextView) view.findViewById(R.id.wallet_amount);

        historyList = (ListView) view.findViewById(R.id.history_list);
        adapter = new HistoryArrayAdapter(getActivity(), new ArrayList<Transaction>(), wallet.getName());
        historyList.setAdapter(adapter);

        payoutButton = (Button) view.findViewById(R.id.payoutButton);
        currencyButton = (Button) view.findViewById(R.id.currencyButton);
        currencyButton.setOnClickListener(this);
        payoutButton.setOnClickListener(this);
        paymentButton = (Button) view.findViewById(R.id.depositButton);
        paymentButton.setOnClickListener(this);

        refreshView();


        return view;
    }

    private void refreshView() {
        if (wallet != null) {
            nameTextView.setText(wallet.getName());
            currencyTextView.setText(wallet.getCurrency());
            amountTextView.setText(wallet.getAmount().toString());
        }
        loadHistory();
    }

    private void loadHistory() {
        new ApiRequest(getActivity(),
                new ApiData(ApiConstants.getTransactions() + wallet.getName(), new HashMap<String, String>(), EHttp
                        .GET, ERequest.TRANSACTIONS_REQUEST), ResponseType.ARRAY, this).execute();
    }

    @Override
    public FragmentType getFragmentType() {
        return WALLET_DETAILS_FRAGMENT;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.payoutButton:
                showPayoutDialog();
                break;
            case R.id.currencyButton:
                getWallets();
                break;
            case R.id.depositButton:
                showPaymentDialog();
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (WalletDetailFragmentCallback) context;
    }

    @Override
    public void onDetach() {
        callback = null;
        super.onDetach();

    }

    public void reload(float amount) {
        float currentAmount = Float.parseFloat(amountTextView.getText().toString());
        float newAmount = currentAmount - amount;
        amountTextView.setText(String.valueOf(newAmount));
    }

    private void getWallets() {
        if(callback != null) {
            callback.onGetWallets();
        }
    }

    public Wallet getCurrentWallet() {
        return wallet;
    }

    public void showTransferDialog(ArrayList<String> wallets) {
        TransferDialog.getInstance(wallet,wallets).show(getFragmentManager(),"");
    }

    private void showPayoutDialog() {
        PayoutDialog.getInstance(wallet).show(getFragmentManager(),"");
    }

    private void showPaymentDialog() {
        PaymentDialog.getInstance(wallet).show(getFragmentManager(), "");
    }

    @Override
    public void onRequestSuccess(ApiResponse response) {
        switch (response.getRequestType()) {
            case GET_WALLET_REQUEST:
                wallet = ApiParser.getWalletFromResponse(response.getResponse());
                refreshView();
                break;
            case TRANSACTIONS_REQUEST:
                try {
                    List<Transaction> values = ApiParser.getTransactionFromResponse(response.getResponse());
                    adapter.clear();
                    adapter.addAll(values);
                } catch (JSONException ex) {
                    Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
                }
        }
    }

    @Override
    public void onRequestFailed(ApiError error) {
        Toast.makeText(getActivity(), error.getErrorMessage(), Toast.LENGTH_LONG).show();
    }

    public void reloadData() {
        new ApiRequest(getActivity(),
                new ApiData(ApiConstants.getGetWallet() + wallet.getName(), new HashMap<String, String>(), EHttp
                        .GET, ERequest.GET_WALLET_REQUEST), ResponseType.OBJECT, this).execute();
    }

}
