package com.angryhamsters.wallet.dialogs;

import android.app.DialogFragment;
import android.os.Bundle;

/**
 * Created by Damian Kwasniak on 09.06.15.
 */
public class ConfirmDialogFragment extends DialogFragment {

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_TITLE,android.R.style.Theme_Holo);
	}
}
