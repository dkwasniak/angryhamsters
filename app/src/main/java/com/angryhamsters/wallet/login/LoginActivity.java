package com.angryhamsters.wallet.login;

import com.angryhamsters.wallet.AppPreferences;
import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.activities.MainActivity;
import com.angryhamsters.wallet.base.BaseActivity;
import com.angryhamsters.wallet.base.BaseFragment;
import com.angryhamsters.wallet.base.FragmentType;
import com.angryhamsters.wallet.base.FragmentsFactory;

import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.Toast;


public class LoginActivity extends BaseActivity {

	private FragmentType currentFragmentType = FragmentType.USER_LOGIN_FRAGMENT;

	private final static String LOGIN_FRAGMENT = "login_fragment";

	private int backPressCounter = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		setContentView(R.layout.activity_login);
		checkIfUserLogged();

		initUI();
	}

	private void checkIfUserLogged() {
		if(!AppPreferences.getInstance().getCookies().equals("")) {
			startActivity(new Intent(this, MainActivity.class));
		}
	}


	@Override
	public void onBackPressed() {
		if (getFragmentManager().getBackStackEntryCount() > 0
				&& getCurrentFragmentType() != FragmentType.USER_LOGIN_FRAGMENT) {
			getFragmentManager().popBackStack();
		} else {
			backPressCounter++;
			if(backPressCounter == 2) {
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			} else {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						backPressCounter=0;
					}
				},1000);
				Toast.makeText(this,R.string.press_again_to_exit,Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void initUI() {
		setMainFragment();
	}


	@Override
	public void setMainFragment() {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(R.id.main_container, FragmentsFactory.getFragmentByType(currentFragmentType),
				LOGIN_FRAGMENT)
				.addToBackStack(LOGIN_FRAGMENT).commit();
	}

	@Override
	public void onChangeFragment(FragmentType type) {
		currentFragmentType = type;
		setMainFragment();
	}

	@Override
	public void onChangeFragment(BaseFragment fragment) {

	}

	private FragmentType getCurrentFragmentType() {
		return ((BaseFragment)(
				getSupportFragmentManager().findFragmentByTag(LOGIN_FRAGMENT))).getFragmentType();
	}
}
