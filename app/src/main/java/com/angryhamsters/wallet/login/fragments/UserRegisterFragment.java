package com.angryhamsters.wallet.login.fragments;

import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.api.ApiConstants;
import com.angryhamsters.wallet.api.ApiData;
import com.angryhamsters.wallet.api.ApiError;
import com.angryhamsters.wallet.api.ApiParser;
import com.angryhamsters.wallet.api.ApiRequest;
import com.angryhamsters.wallet.api.ApiResponse;
import com.angryhamsters.wallet.api.enumeration.EHttp;
import com.angryhamsters.wallet.api.enumeration.ERequest;
import com.angryhamsters.wallet.api.enumeration.ResponseType;
import com.angryhamsters.wallet.api.models.User;
import com.angryhamsters.wallet.base.BaseFragment;
import com.angryhamsters.wallet.base.FragmentType;
import com.angryhamsters.wallet.utils.GlobalUtils;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Map;

import static com.angryhamsters.wallet.base.FragmentType.USER_REGISTER_FRAGMENT;
import static java.util.Arrays.asList;

public class UserRegisterFragment extends BaseFragment implements View.OnClickListener,
		ApiRequest.OnNetworkListener {

	private EditText passwordEditText;

	private EditText repeatPasswordEditText;

	private EditText userNameEditText;

	private Button registerButton;

	public static UserRegisterFragment newInstance() {
		UserRegisterFragment fragment = new UserRegisterFragment();
		Bundle args = new Bundle();
		return fragment;
	}

	public UserRegisterFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_user_register, container, false);

		userNameEditText = (EditText) view.findViewById(R.id.userNameTextView);
		passwordEditText = (EditText) view.findViewById(R.id.passwordEditText);
		repeatPasswordEditText = (EditText) view.findViewById(R.id.repeatPasswordEditText);
		registerButton = (Button) view.findViewById(R.id.registerButton);
		registerButton.setOnClickListener(this);

		return view;
	}

	@Override
	public void onRequestSuccess(final ApiResponse response) {
		baseFragmentListener.onChangeFragment(FragmentType.USER_LOGIN_FRAGMENT);
		System.out.println("");
	}

	@Override
	public void onRequestFailed(final ApiError error) {
		Toast.makeText(getActivity(), error.getErrorMessage(), Toast.LENGTH_LONG).show();
	}

	@Override
	public FragmentType getFragmentType() {
		return USER_REGISTER_FRAGMENT;
	}


	private void registerUser() {
		Map<String, String> params = GlobalUtils.getRequestParams(
				asList("password", "username"),
				asList(passwordEditText.getText().toString(),
						userNameEditText.getText().toString()));
		new ApiRequest(getActivity(),
				new ApiData(ApiConstants.getUserRegister(), params, EHttp
						.POST, ERequest.USER_REGISTER_REQUEST), ResponseType.OBJECT, this).execute();
	}


	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
			case R.id.registerButton:
				registerUser();
				break;
		}
	}
}
