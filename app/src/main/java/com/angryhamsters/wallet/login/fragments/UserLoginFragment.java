package com.angryhamsters.wallet.login.fragments;

import com.angryhamsters.wallet.R;
import com.angryhamsters.wallet.activities.MainActivity;
import com.angryhamsters.wallet.api.ApiConstants;
import com.angryhamsters.wallet.api.ApiData;
import com.angryhamsters.wallet.api.ApiError;
import com.angryhamsters.wallet.api.ApiRequest;
import com.angryhamsters.wallet.api.ApiResponse;
import com.angryhamsters.wallet.api.enumeration.EHttp;
import com.angryhamsters.wallet.api.enumeration.ERequest;
import com.angryhamsters.wallet.api.enumeration.ResponseType;
import com.angryhamsters.wallet.base.BaseFragment;
import com.angryhamsters.wallet.base.FragmentType;
import com.angryhamsters.wallet.utils.GlobalUtils;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;

import static com.angryhamsters.wallet.base.FragmentType.USER_LOGIN_FRAGMENT;
import static java.util.Arrays.asList;

public class UserLoginFragment extends BaseFragment implements OnClickListener, ApiRequest
		.OnNetworkListener {

	private EditText phoneEditText;

	private EditText passwordEditText;

	private Button loginButton;

	private TextView signupTextView;

	private TextView forgotPasswordTextView;


	public static UserLoginFragment newInstance() {
		UserLoginFragment fragment = new UserLoginFragment();
		return fragment;
	}

	public UserLoginFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_user_login, container, false);

		passwordEditText = (EditText) view.findViewById(R.id.passwordEditText);
		phoneEditText = (EditText) view.findViewById(R.id.phoneTextView);
		forgotPasswordTextView = (TextView) view.findViewById(R.id.forgotPasswordTextView);
		loginButton = (Button) view.findViewById(R.id.loginButton);
		signupTextView = (TextView) view.findViewById(R.id.signUpTextView);

		// register listeners
		signupTextView.setOnClickListener(this);
		forgotPasswordTextView.setOnClickListener(this);
		loginButton.setOnClickListener(this);


		passwordEditText.setImeActionLabel(getResources().getString(R.string.log_in), KeyEvent
				.KEYCODE_ENTER);
		passwordEditText.setOnEditorActionListener(new EditText.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(final TextView v, final int actionId,
					final KeyEvent event) {
				return false;
			}
		});

		return view;
	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
			case R.id.loginButton:
				logIn();
				break;
			case R.id.signUpTextView:
				signUp();
				break;
			case R.id.forgotPasswordTextView:
				forgotPassword();
				break;
		}
	}

	@Override
	public FragmentType getFragmentType() {
		return USER_LOGIN_FRAGMENT;
	}

	@Override
	public void onRequestSuccess(final ApiResponse response) {
		startActivity(new Intent(getActivity(), MainActivity.class));
	}

	@Override
	public void onRequestFailed(final ApiError error) {
		Toast.makeText(getActivity(), error.getErrorMessage(), Toast.LENGTH_LONG).show();
	}

	private void logIn() {
		Map<String, String> params = GlobalUtils.getRequestParams(
				asList("password", "username"),
				asList(passwordEditText.getText().toString(),
						phoneEditText.getText().toString()));
		new ApiRequest(getActivity(),
				new ApiData(ApiConstants.getUserLogin(), params, EHttp
						.POST, ERequest.USER_LOGIN_REQUEST), ResponseType.OBJECT, this).execute();
	}

	private void signUp() {
		baseFragmentListener.onChangeFragment(FragmentType.USER_REGISTER_FRAGMENT);
	}

	private void forgotPassword() {

	}


}
